main: ds_tree.o ds_exp.o main.o
	g++ -o main ds_tree.o ds_exp.o main.o -lcurses

ds_tree.o: ds_tree.cpp
	g++ -c ds_tree.cpp

ds_exp.o: ds_exp.cpp
	g++ -c ds_exp.cpp

main.o: main.cpp
	g++ -c main.cpp

clean:
	rm -f ds_tree.o ds_exp.o main.o main
