#include <stack>
#include <string>

#ifndef	_DS_EXP_H_
#define	_DS_EXP_H_

#include "./ds_tree.h"

#define		MAX_EXPLEN		64
#define		is_sym(c) ((c >= 'a') && (c <= 'z'))?true:false

tree* ds_exp_post2tree(const char *exp);

const char* ds_exp_in2post(char *dst,const char *src);

#endif