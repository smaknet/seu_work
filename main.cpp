#include "./ds_exp.h"

int main(int argc,char **argv)
{
	if(argc < 2)
		return -1;

	char c,exp[MAX_EXPLEN];

	ds_exp_in2post(exp,argv[1]);
	tree *r = ds_exp_post2tree(exp);

	printf("prefix expression is ");
	ds_tree_preorder(r);
	printf("\n");
	printf("infix expression is %s\n",argv[1]);
	printf("postfix expression is %s\n",exp);

	printf("print the expression tree?[y/n]: ");
	if((c=getchar()) == 'y') {
		//printf("print tree starting...\n");
		ds_tree_print(r);
		printf("print tree ending...\n");
	}

	return 0;
}