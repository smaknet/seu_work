#ifndef	_DS_TREE_H_
#define	_DS_TREE_H_

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include <malloc.h>
#include <memory.h>

extern int MAXCOLS;

typedef struct tree_s	tree;
struct tree_s{
	char data;
	tree *lhs;
	tree *rhs;
};

tree* ds_tree_init(char c);

void ds_tree_preorder(tree *t);
void ds_tree_inorder(tree *t);
void ds_tree_postorder(tree *t);

int ds_tree_print(tree *t);

#endif