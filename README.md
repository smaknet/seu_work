## Introduction ##
This document will introduce my project
You can connect with me by qinyang@seu.edu.cn

## The code ##
The codes are written in **SEU**(south-east university),most of them aimed to finish lesson. I share them and welcome everyone to check them.

## How it works ##
You can get the work:

```
#!bash

git clone https://smaknet@bitbucket.org/smaknet/seu_work.git
```
and compile them:

```
#!bash

cd seu_work
make
```