#include "./ds_tree.h"

#define		LEFT_POS(x,offset)	x-offset	
#define		RIGHT_POS(x,offset)	x+offset

int MAXCOLS;	//the max columes of the screen

tree* ds_tree_init(char c)
{
	tree *t = (tree*)malloc(sizeof(tree));
	if(!t) {
		perror("malloc memory failed...\n");
		return NULL;
	}

	memset(t,0,sizeof(tree));
	t->data = c;

	return t;
}

void ds_tree_preorder(tree *t)
{
	if(t) {
		printf("%c",t->data);
		ds_tree_preorder(t->lhs);
		ds_tree_preorder(t->rhs);
	}
}

void ds_tree_inorder(tree *t)
{
	if(t) {
		ds_tree_inorder(t->lhs);
		printf("%c",t->data);
		ds_tree_inorder(t->rhs);
	}
}

void ds_tree_postorder(tree *t)
{
	if(t) {
		ds_tree_postorder(t->lhs);
		ds_tree_postorder(t->rhs);
		printf("%c",t->data);
	}
}

void ds_tree_node_print(tree *t,int line,int col,int h)
{
	if(!t)
		return;

	move(line++,col-1);
	addch('(');
	addch(t->data);
	addch(')');

	int offset = MAXCOLS/pow(2,++h);

	ds_tree_node_print(t->lhs,line,LEFT_POS(col,offset),h);
	ds_tree_node_print(t->rhs,line,RIGHT_POS(col,offset),h);
}

int ds_tree_print(tree *t)
{
	int line;

	initscr();
	clear();
	getmaxyx(stdscr,line,MAXCOLS);
	ds_tree_node_print(t,0,MAXCOLS/2,1);
	move(line, MAXCOLS);
	refresh();
	getch();
	endwin();
}