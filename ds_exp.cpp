#include "./ds_exp.h"

int level(char c) 
{
	switch(c) {
		case '=':
			return -1;
		case ')':
			return 0;
		case '+':
		case '-':
			return 1;
		case '*':
		case '/':
			return 2;
		case '(':
			return 3;
	}
}

const char* ds_exp_in2post(char *dst,const char *src)
{
	int i,j;
	std::stack<char> ss;

	for(i=0,j=0;i<strlen(src);++i) {
		if(is_sym(src[i]))
			dst[j++] = src[i];
		else {
			if(src[i] == ')') {
				while(!ss.empty() && ss.top()!='(') {
					dst[j++] = ss.top();
					ss.pop();
				}
				ss.pop();
			}else {
				while(!ss.empty() && ss.top() != '(' && level(ss.top()) >= level(src[i])) {
					dst[j++] = ss.top();
					ss.pop();
				}
				ss.push(src[i]);
			}
		}
	}
	while(!ss.empty()) {
		dst[j++] = ss.top();
		ss.pop();
	}

	dst[j] = '\0';
	return dst;
}

tree* ds_exp_post2tree(const char *exp)
{
	std::stack<tree*> ss;

	for(int i = 0;i<strlen(exp);++i) {
		if(is_sym(exp[i])) {
			tree *sym = ds_tree_init(exp[i]);
			ss.push(sym);
		}else {
			tree *t1 = ss.top();
			ss.pop();
			tree *t2 = ss.top();
			ss.pop();

			tree *op = ds_tree_init(exp[i]);
			op->lhs = t2;
			op->rhs = t1;

			ss.push(op);
		}
	}

	tree *r = ss.top();
	ss.pop();

	return r;
}